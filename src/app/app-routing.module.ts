import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { JogarPageComponent } from './jogar-page/jogar-page.component';
import {AdminPageComponent} from './admin-page/admin-page.component';

const routes: Routes = [
  { path: '', redirectTo: 'jogar', pathMatch: 'full' },
  { path: 'jogar', component: JogarPageComponent },
  { path: 'admin/login', component: AdminPageComponent },
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
