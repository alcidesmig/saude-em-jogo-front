import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JogarPageComponent } from './jogar-page.component';

describe('JogarPageComponent', () => {
  let component: JogarPageComponent;
  let fixture: ComponentFixture<JogarPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JogarPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JogarPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
