export class MenuItem {
  icon: string;
  label: string;
  href: string;
}
